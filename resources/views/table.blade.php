<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>

    <div class="containe-fluid">

    <table class="table table-striped table-hover">
        <thead>
            <tr>

                <th scope="col">device</th>
                <th scope="col">dia</th>
                <th scope="col">perido noturno</th>
                <th scope="col">rega alocada</th>
                <th scope="col">rega regular</th>
                <th scope="col">total consumption</th>
                <th scope="col">dentro quota</th>
                <th scope="col">fora quota</th>
                <th scope="col">quota max</th>
                <th scope="col">total a pagar</th>

            </tr>
        </thead>
        <tbody>
                @foreach ($data as $key=> $item)
                <tr>
                   
                    <td>{{ $item->device }}</td>
                    <td>{{ $item->dia }}</td>
                    
                    <td>{{ $item->periodo_noturno }}</td>
                    <td>
                        {{ $item->alocado }}
                       
                    </td>
                    <td>{{ $item->rega_regular }}</td>
                    <td>{{ $item->total_consumption }}</td>
                    <td>{{ $item->dentro_quota }}</td>
                    <td>{{ $item->fora_quota }}</td>
                    <td>{{ $item->quota_max }}</td>
                    <td>{{ $item->total_pagar }}</td>
                    
                    <td class="table-warning">{{ $item->hora0 }}</td>
                    <td class="table-warning">{{ $item->valor0 }}</td>
                    
                    <td class="table-warning">{{ $item->hora1 }}</td>
                    <td class="table-warning">{{ $item->valor1 }}</td>
                    
                    <td class="table-warning">{{ $item->hora2 }}</td>
                    <td class="table-warning">{{ $item->valor2 }}</td>

                    <td class="table-warning">{{ $item->hora3 }}</td>
                    <td class="table-warning">{{ $item->valor3 }}</td>

                    <td class="table-warning">{{ $item->hora4 }}</td>
                    <td class="table-warning">{{ $item->valor4 }}</td>

                    <td class="table-warning">{{ $item->hora5 }}</td>
                    <td class="table-warning">{{ $item->valor5 }}</td>

                    <td class="table-warning">{{ $item->hora6 }}</td>
                    <td class="table-warning">{{ $item->valor6 }}</td>

                    <td class="table-warning">{{ $item->hora7 }}</td>
                    <td class="table-warning">{{ $item->valor7 }}</td>

                    <td>{{ $item->hora8 }}</td>
                    <td>{{ $item->valor8 }}</td>

                    <td>{{ $item->hora9 }}</td>
                    <td>{{ $item->valor9 }}</td>

                    <td>{{ $item->hora10 }}</td>
                    <td>{{ $item->valor10 }}</td>

                    <td>{{ $item->hora11 }}</td>
                    <td>{{ $item->valor11 }}</td>

                    <td>{{ $item->hora12 }}</td>
                    <td>{{ $item->valor12 }}</td>

                    <td>{{ $item->hora13 }}</td>
                    <td>{{ $item->valor13 }}</td>

                    <td>{{ $item->hora14 }}</td>
                    <td>{{ $item->valor14 }}</td>

                    <td>{{ $item->hora15 }}</td>
                    <td>{{ $item->valor15 }}</td>

                    <td>{{ $item->hora16 }}</td>
                    <td>{{ $item->valor16 }}</td>

                    <td>{{ $item->hora17 }}</td>
                    <td>{{ $item->valor17 }}</td>

                    <td>{{ $item->hora18 }}</td>
                    <td>{{ $item->valor18 }}</td>

                    <td>{{ $item->hora19 }}</td>
                    <td>{{ $item->valor19 }}</td>

                    <td>{{ $item->hora20 }}</td>
                    <td>{{ $item->valor20 }}</td>

                    <td class="table-warning">{{ $item->hora21 }}</td>
                    <td class="table-warning">{{ $item->valor21 }}</td>

                    <td class="table-warning">{{ $item->hora22 }}</td>
                    <td class="table-warning">{{ $item->valor22 }}</td>
                    
                    <td class="table-warning">{{ $item->hora23 }}</td>
                    <td class="table-warning">{{ $item->valor23 }}</td>

                    
                    

                </tr>
                @endforeach
            </tbody>
    </table>
    </div>
</body>

</html>
