<html>
<head>
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> --}}
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> --}}
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> --}}
    <style>
        .flex{
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }

        div td p {
            font-size:12px;
            margin-top:0px;
        }
        div td  {
            margin-top:5px;
            font-size:12px;
        }
        h2 {
            font-size:18px;
            margin-top:30px;
            margin-bottom:15px;
        }
        h3{
            text-align: left;
            font-size: 20px;
            font-weight: bold;
            letter-spacing: 0px;
            color: #282727;
            opacity: 1;
        }
       
        .container {
            width: 100%;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        font-weight:normal;
        }
        .table-form{
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            table-layout: auto; 
        }
        .form th {
        border: 0px;  
        text-align: left;
        word-wrap: none;
        word-wrap: break-word;   
        overflow-wrap: break-word;
        margin-top: 30px
        }
        .sub-container {
        margin-bottom: 20px;
        margin-top: 20px;
        }
        #total_price {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <div class="container">

        <div>
            <div style="justify-content: center;align-self: center; padding-bottom: 30px;">
                <div>
                    <img src={{url($data['logo_url'])}} alt="Logo" style="width: 200px; height: 80px;">
            </div>
        </div>


        <?php $fromDate = $data['fromDate']; ?>
        <?php $toDate = $data['toDate']; ?>
        <?php $frequency = $data['frequency']; ?>
        <?php $group_by = isset($data['group_by']) ? $data['group_by'] : 'Dispositivo'; ?>
        <?php $consumptions = $data['consumptions']; ?>
        <?php $client = $data['client']; ?>
        <?php $scale = $data['scale']; ?>

        <?php $total_price = $data['total_price']; ?>

        <div class="sub-container">
            <div>
                <h3>Cliente</h3>
            </div>
            <table class="table-form">
                <tr class="form">
                    <th >Nome: {{$client['name']}}</th>
                </tr> 
                <tr class="form">
                    <th >Nif: {{$client['nif']}}</th>
                </tr> 
                <tr class="form">
                    <th >Email: {{$client['email']}}</th>
                </tr> 
                <tr class="form">
                    <th >Número de telemóvel: {{$client['phone']}}</th>
                </tr> 
                <tr class="form">    
                    <th >Morada: {{$client['address']}}</th>
                </tr>
                <tr class="form">
                    <th >Dias de semana permitidos para consumo: {{implode(",", array_map(function ($item) {
                        return \App\Utils\Utils::transformDayWeekToString($item);
                    }, json_decode($client['allowed_weekday'])))}}</th>
                </tr>
            </table>

        </div>


        @if($scale != null)
            <div class="sub-container">
                <div>
                    <h3>Escala</h3>
                </div>

                <table class="table-form">
                    <tr class="form">
                        <th >Nome: {{$scale['name']}}</th>
                    </tr> 
                    <tr class="form">
                        <th >Permilagem mínima: {{$scale['permilage_min']}}</th>
                        <th >Permilagem máxima: {{$scale['permilage_max']}}</th>
                    </tr> 
                    <tr class="form">
                        <th >Quota diária: {{$scale['quota_day']}}</th>
                        <th >Plafond: {{$scale['plafond']}}</th>
                        <th >Tipo de quota: {{$scale['quota_type']}}</th>
                    </tr> 

                </table>


        @endif

        <div class="sub-container">
            <div>
                <h3>Detalhes do consumo</h3>
            </div>
            <table class="table-form">
                <tr class="form">
                    <th >Data de início: {{$fromDate}}</th>
                    <th >Data de fim: {{$toDate}}</th>
                </tr> 
                <tr class="form">
                    <th >Frequência: {{$frequency}}</th>
                    <th >Agrupado por: {{$group_by}}</th>
                </tr>
            </table>

        </div>

        <div id="total_price" style="overflow: hidden; width: 100%">
            <div style="">
                <p style="text-align: left;font-size: 25px;font-weight: bold;letter-spacing: 0.01px;opacity: 1;">Total a pagar: {{$total_price}}€</p>
            </div>
        </div>

        <div class="sub-container">
            <div>
                <h3>Resumo Consumos</h3>
            </div>
           
            @if($consumptions != null)
                <table>
                    <tr>
                        <th style = "font-size: 10px;">Dispositivos</th>
                        <th style = "font-size: 10px;">Consumo total (m3)</th>
                        <th style = "font-size: 10px;">Consumo total (L)</th>
                        <th style = "font-size: 10px;">Quota mínima (m3)</th>
                        <th style = "font-size: 10px;">Consumo inferior a 50% (m3)</th>
                        <th style = "font-size: 10px;">Consumo superior a 50% (m3)</th>
                        <th style = "font-size: 10px;">Preço quota mínima (€)</th>
                        <th style = "font-size: 10px;">Preço consumo inferior a 50% (€)</th>
                        <th style = "font-size: 10px;">Preço consumo superior a 50% (€)</th>
                        <th style = "font-size: 10px;">Preço total (€)</th>
                        <th style = "font-size: 10px;">Data de início</th>
                        <th style = "font-size: 10px;">Data de fim</th>
                    </tr>
                @foreach ($consumptions as $cc)
                    <tr>
                        <td>{{isset($cc->device_name) ? $cc->device_name : "Todos"}}</td>
                        <td>{{$cc->total_consumption_m3}}</td>
                        <td>{{$cc->total_consumption_liter}}</td>
                        <td>{{$cc->min_quota}}</td>
                        <td>{{$cc->less_than_50percent}}</td>
                        <td>{{$cc->more_than_50percent}}</td>
                        <td>{{$cc->price_min_quota}}</td>
                        <td>{{$cc->price_less_than_50percent}}</td>
                        <td>{{$cc->price_more_than_50percent}}</td>
                        <td>{{$cc->total_price}}</td>
                        <td>{{$cc->start_date}}</td>
                        <td>{{$cc->end_date}}</td>
                        
                    </tr>
                    <?php $total_price += $cc->total_price; ?>
                @endforeach
            </table>
            @endif      
        </div>
        <hr>
</body>
</html>
