<?php
    $Contract = json_decode(file_get_contents($json), true);
?>

<html>
<head>
    <style>
        body {
            display: flex;
            flex-flow: column wrap;
            padding: 10px 15px;
            font-size: 24px;
            position: relative;
            align-items: flex-start;
            line-height: 35px;
        }
        .maintitle {
            margin-left: 40%;
        }
        .second > b {
            font-weight: bold;
            margin: auto;
        }
        ul {
            list-style: square;
        }
        .last {
            margin-left: 80%;
        }
        .upimage {
            display: flex;
            flex-flow: row nowrap;
            align-self: flex-end;
            display: block;
        }
        .image {
            display: flex;
            flex-flow: row nowrap;
            justify-content: space-between;
            align-self: flex-end;
            display: block;
        }
        .right {
            align-self: flex-end;
            margin-left: 60% ; 
        }
        .left {
            align-self: flex-start;
        }
        img {
            width :150px;
            height: 100px;
        }
    </style>
    <title>Contract</title>
</head>
    <body>
        <h1 class="maintitle">{{ $Contract["title"] }}</h1>
        <br/><br />
        <div class="upimage right">
            <img class="up" src={{ public_path() . $Contract["logos"]["img1"] }} >
            <img class="up" src={{ public_path() . $Contract["logos"]["img1"] }} >
        </div>
<!-- ############################################################################ -->
    <?php $logo = $Contract["logos"] ?>
    <?php $Participant_1 = $Contract["participants"]["Participant_1"] ?>
    <?php $Participant_2 = $Contract["participants"]["Participant_2"] ?>
    <?php $Time = $Contract["Time"] ?>
    <?php $other = $Contract["Term_2"] ?>
    <?php $page = $Contract["page"] ?>
<!-- ############################################################################ -->
        <div class="first">
            <b><b style="text-transform: lowercase;">l</b>. The Parties.</b> This Service Contract ("Agreement") made {{ $Time["date"] }}, 20 {{ $Time["year"] }} ("Effective Date"), is by and between:
        </div>
        <br />
        <div class="second">
        <b>Service Provider:</b>
            {{ $Participant_1["name"] }} with a mailing address {{ $Participant_1["address"] }} of City of {{ $Participant_1["city"] }} - State of {{ $Participant_1["state"] }}(Service Provider),
        </div>
        <br/>
        AND
        <br />
    <br />
    <div class="third">
    <b>Client:</b>
        {{ $Participant_2["name"] }}, with a mailing address of {{ $Participant_2["address"] }} city of {{ $Participant_2["city"] }} State of {{ $Participant_2["state"] }}
        ("Client"),
    </div>
    <br/>
    <div class="fourth">
        Service Provider and Client are each referred to herein as a "Party and, collectively, as the
        "Parties."
    </div>
    <br/>
    <div class="fifth">
        NOW, THEREFORE, FOR AND IN CONSIDERATION of the mutual promises and agreements contained herein, the
        Client hires the Service Provider to work under the terms and conditions hereby agreed upon by the
        Parties:
    </div>
    <br/>
    <div class="sixth">
        <B>II. Term.</B> The term of this Agreement shall commence on {{ $Time["date"] }} 20 {{ $Time["year"] }}and terminate: (check one)
    </div>
        <ul>
            <li>- At-Will: Written notice of at least {{ $Time["days"] }} days' notice.</li>
            <li>- End Date: On {{ $Time["deadline"] }}, 20{{ $Time["deadline_year"] }}.</li>
            <li>- Other: {{ $Time["data"]["Text"] }}.</li>
        </ul>
        <div>
            III. The Service. The Service Provider agrees to provide the following:
        </div>
        <br/>
        <div>
            {{ $other["data"]["Text"] }}
        </div>
    <br/>
    <div class="eighth">
        Hereinafter known as the "Service".
    </div>
    <br/>
    <div class="ninth">
        Service Provider shall provide, while providing the Service, that he/she/they shall comply with the
        policies, standards, and regulations of the Client, including local, State, and Federal laws and to
        the best of their abilities,
    </div>
    <br/>
    <div class="tenth">
        <B>IV. Payment Amount.</B> The Client agrees to pay the Service Provider the following compensation for the
        Service performed under this Agreement: (check one)
    </div>
    <br/>
    <ul>
        <li>- $ {{ $other["salary"]["per_hour"] }} /Hour</li>
        <li>- $ {{ $other["salary"]["salary"] }}/ per Job. A "Job" is {{ $other["data"]["notes"] }}</li>
        <li>- Other: {{ $other["data"]["Text"] }}</li>
    </ul>
    <div class="image">
        <img class="down left" src={{ public_path() . $Contract["logos"]["img1"] }} >
        <img class="down right" src={{ public_path() . $Contract["logos"]["img1"] }} >
    </div>
    <i class={{ $logo["img3"] }}></i>
    <div class="last">Page {{ $page["page"] }} of {{ $page["pages"] }}</div>
    <br/>
    </body>
</html>