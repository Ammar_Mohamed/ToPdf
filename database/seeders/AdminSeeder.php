<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!$user = \App\Models\User::where('email', 'admin@mutablep.com')->first()){
            \App\Models\User::updateOrCreate([
                'name' => 'Admin',
                'email' => 'admin@mutablep.com',
                'role' => 'admin',
                'password' => bcrypt('q1w2e3r4t5!mutablep'),
            ]);
            $user = \App\Models\User::where('email', 'admin@mutablep.com')->first();
    
        }else{
            $user->update([
                'name' => 'Admin',
                'role' => 'admin',
                'password' => bcrypt('q1w2e3r4t5!mutablep')
            ]);
        }
    }
}
