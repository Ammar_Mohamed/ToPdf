<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/health', function () {
    return response()->json([
        'success' => true,
        'message' => 'OK'
    ], 200);
});


Route::group(['prefix' => 'v1', 'middleware' => 'api'],function() {
    Route::post('/login', 'App\Http\Controllers\AuthController@login');
    Route::get('/logout', 'App\Http\Controllers\AuthController@logout');
    Route::group(['prefix' => 'users', 'middleware' => ['jwt.verify']], function() {
        Route::get('/list', 'App\Http\Controllers\UsersController@index');
        Route::get('/{id}', 'App\Http\Controllers\UsersController@show');
        Route::post('/create', 'App\Http\Controllers\UsersController@store');
        Route::patch('/update/{id}', 'App\Http\Controllers\UsersController@update');
        Route::delete('/delete/{id}', 'App\Http\Controllers\UsersController@destroy');
    });
});