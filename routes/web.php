<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\MyWater\Consumptions;

use Illuminate\Support\Facades\App; 
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# logo route
Route::get('/logo', function () {

    $file = public_path() . '/images/logo.png';
    $headers = array(
        'Content-Type: image/png',
    );
    return response()->download($file, 'logo.png', $headers);
});

#pdf route
Route::get('/pdf' , function () {
    $path = public_path()."/data.json"; 

    $pdf_view = PDF::loadView('pdfs/Contract' , ['json' => $path]);

    // $snappy = App::make('snappy.image');

    $html = view('pdfs/Contract' , ['json' => $path])->render();

    $nameImage = 'Contract.jpg';

    // $snappy->generateFromHtml($html,public_path($nameImage));

    base64_encode(QrCode::format('png')->merge(public_path('images/logo.png'), .3, true)->size(200)->generate('http://127.0.0.1:8000/pdf/convert', public_path('qrcode.png')));

    $pdf_view->save(public_path('Contract.pdf'));

});

