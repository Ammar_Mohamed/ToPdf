<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Interfaces\ProfileInterface;

class User extends ProfileInterface
{
    use HasApiTokens, HasFactory, Notifiable;

    // Rest omitted for brevity


    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];




    public static function roles(){
        return [
            'admin',
            'client'
        ];
    }

    public function isAdmin() {
        return $this->role == 'admin';
    }

    public static function createOne($data){

        try {
            $user = new self();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = \Hash::make($data['password']);
            $user->role = $data['role'];
            $user->save();
            $user->refresh();
    

            return $user;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return null;
        }



    }

    public function updateOne($data){
        try {
            $this->name = $data['name'];
            $this->email = $data['email'];
            $this->role = $data['role'];
            $this->save();
            $this->refresh();

            return $this;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return null;
        }

    }



}
