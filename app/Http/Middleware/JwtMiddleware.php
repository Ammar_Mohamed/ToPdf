<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
        * Handle an incoming request.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  \Closure  $next
        * @return mixed
        */
    public function handle($request, Closure $next)
    {


        
        try {
            $token = request()->header('Authorization');

            if("Bearer " != substr($token, 0, 7)){
                $token = "Bearer ".$token;
                request()->headers->set('Authorization', $token);
            }
            

            if(!auth('usersapi')->user()){
                return response()->json([
                    'success' => false,
                    'jwtExpired' => true,
                    'message' => 'User not found'
                ], 404);
            }

        } catch (Exception $e) {
            \Log::info("Request url: ".request()->fullUrl());
            \Log::info("Request: ".json_encode(request()->all()));
            \Log::error($e);
            \Log::emergency($e->getMessage());
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json([
                    'success' => false, 
                    'jwtExpired' => true,
                    'message' => 'Token is Invalid'
                ], 419);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json([
                    'success' => false, 
                    'jwtExpired' => true,
                    'message' => 'Token is Expired'
                ], 419);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException){
                return response()->json([
                    'success' => false, 
                    'jwtExpired' => true,
                    'message' =>'Token is Blacklisted',
                ], 419);
            }else{
                return response()->json([
                    'success' => false, 
                    'jwtExpired' => true,
                    'message' => 'Authorization Token not found'
                ], 419);
            }
        }
        return $next($request);
    }
}