<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{

    private $entityModel = "\App\Models\User";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Log::info("UsersController::index()");
        $page = request()->page ? intval(request()->page) : 1;
        $perPage = request()->perPage ? intval(request()->perPage) : 10;
        $orderBy = request()->orderBy ? request()->orderBy : 'id';

        $users = $this->entityModel::orderBy($orderBy, 'desc')->skip(($page - 1) * $perPage)->take($perPage)->get();
        $users = $users->map(function ($item) {
            $item->permissions = $item->permissions()->get()->pluck('name');
            return $item;
        });

        \Log::info("UsersController::index() - users: " . json_encode($users));
        return response()->json([
            'success' => true,
            'result' => $users,
            'pagination' => [
                'count' => ceil($this->entityModel::count()),
                'page' => $page,
            ],
        ], 200);

    }

    // search

    public function search()
    {
        \Log::info("UsersController::search()");
        $search = request()->q ? request()->q : '';
        $fields = request()->fields ? request()->fields : 'name';

        $users = $this->entityModel::where($fields, 'like', '%' . $search . '%')->get();

        $users->map(function ($user) {
            $user->permissions = ["admin", "user"];
        });

        return response()->json([
            'success' => true,
            'result' => $users

        ], 200);
    }


    public function show($id)
    {
        $user = $this->entityModel::find($id);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
        }
        return response()->json([
            'success' => true,
            'result' => $user
        ], 200);
    }


    // store
    public function store(Request $request)
    {
        $data = $request->all();

        $user = $this->entityModel::createOne($data);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not added'
            ], 500);
        }


        return response()->json([
            'success' => true,
            'result' => $user
        ], 200);
    }
    
    
    // update
    
    public function update(Request $request, $id)
    {
        $user = $this->entityModel::find($id);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
        }
        $user = $user->updateOne($request->all());
        if(!$user){
            return response()->json([
                'success' => false,
                'message' => 'User not updated'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'result' => $user
        ], 200);
    }


    
    // destroy

    public function destroy($id)
    {
        $user = $this->entityModel::find($id);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
        }
        \App\Models\AdminPermissions::where('user_id', $id)->delete();
        $user->delete();
        return response()->json([
            'success' => true,
            'result' => $user
        ], 200);
    }


    public function permissions(){
        return response()->json([
            'success' => true,
            'result' =>  \App\Models\AdminPermissions::permissions()
        ], 200);
    }


    public function roles(){
        return response()->json([
            'success' => true,
            'result' => $this->entityModel::roles()
        ], 200);
    }


}
