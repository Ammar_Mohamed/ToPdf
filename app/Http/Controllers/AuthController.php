<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'respondWithToken']]);
    }

    // login
    // logout


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

        /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithToken($token, $auth)
    {
        return response()->json([
            'success' => true,
            'result' => [
                'profile' => auth($auth)->user(),
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ],
            'message' => 'Welcome back!',
        ], 200);


    }




    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $email = $request->input('email');
        $user = \App\Models\Profile::getForEmail($email);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found',
            ], 404);
        }

        $auth = 'usersapi';
        if($user->profile_type == 'client') {
            $auth = 'clientsapi';
        }

        if (!$token = auth($auth)->attempt($credentials)) {

            return response()->json([
                'success' => false,
                'message' => 'Password incorrect!',
            ], 401);
        }
        return $this->respondWithToken($token, $auth);
    }

    public function logout()
    {
        auth()->logout();
        return response()->json([
            'success' => true,
            'message' => 'Successfully logged out',
        ]);
    }

}

