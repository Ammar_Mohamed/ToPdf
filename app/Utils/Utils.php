<?php

namespace App\Utils;
use Carbon\Carbon;



class Utils
{

    public static function getDatesFromRange($date_time_from, $date_time_to, $days = 1, $format = 'd-m-Y')
    {

        // cut hours, because not getting last day when hours of time to is less than hours of time_from
        // see while loop
        $start = Carbon::parse($date_time_from)->startOfDay();
        $end = Carbon::parse($date_time_to)->endOfDay();

        $dates = [];

        while ($start->lte($end)) {

            $start_date = $start->copy()->format($format);

            $end_date = $start->addDays($days);

            $dates[] = (object)[
                'start_date' => $start_date,
                'end_date' => $end_date->format($format),
            ];

            $start = $end_date->copy();

        }

        return $dates;
    }

}