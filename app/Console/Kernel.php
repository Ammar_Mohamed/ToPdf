<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();


        $schedule->call(function () {

            $cached_schedule_trigger = \Cache::get('schedule_trigger_seedConsumptions'.date('Y-m-d'));
            if($cached_schedule_trigger == null){
                \Cache::put('schedule_trigger_seedConsumptions'.date('Y-m-d'), true, 60*60*24);
                try {
                    $today = \Carbon\Carbon::now();
                    $last_consumption = \App\Models\Consumption::orderBy('id', 'desc')->first();
                    $last_consumption_date = null;
                    $today_date = null;
                    if($last_consumption != null){
                        $last_consumption_date = \Carbon\Carbon::parse($last_consumption->data)->format('Y-m-d');
                        $today_date = $today->format('Y-m-d');
                    }
    
                    \App\Models\Device::seedDevices();
                    \App\Models\Consumption::seedConsumptions($last_consumption_date, $today_date);
    
    
                } catch (\Exception $e) {
                    \Log::emergency('[Kernel schedule] '.$e);
                }
                \Cache::forget('schedule_trigger_seedConsumptions'.date('Y-m-d'));
            }

        })->at('00:00')->timezone('Europe/Lisbon');

        $schedule->call(function () {
            
            $cached_schedule_trigger = \Cache::get('schedule_trigger_generateMissingBins'.date('Y-m-d'));
            if($cached_schedule_trigger == null){
                \Cache::put('schedule_trigger_generateMissingBins'.date('Y-m-d'), true, 60*60*24);

                try {
                    \App\Models\Consumption::generateMissingBins();
                } catch (\Exception $e) {
                    \Log::emergency('[Kernel schedule] '.$e);
                }

                \Cache::forget('schedule_trigger_generateMissingBins'.date('Y-m-d'));
            }

        })->at('03:00')->timezone('Europe/Lisbon');


        $schedule->call(function () {
            $cached_schedule_trigger = \Cache::get('schedule_trigger_sendMonthlyConsumption'.date('Y-m-d'));
            if($cached_schedule_trigger == null){
                \Cache::put('schedule_trigger_sendMonthlyConsumption'.date('Y-m-d'), true, 60*60*24);

                try {
                    \App\Models\Consumption::sendMonthlyConsumption();
                } catch (\Exception $e) {
                    \Log::emergency('[Kernel schedule] '.$e);
                }

                \Cache::forget('schedule_trigger_sendMonthlyConsumption'.date('Y-m-d'));
            }

        })->at('01:00')->when(function () {
            return \Carbon\Carbon::parse('first day of this month')->format('Y-m-d') == \Carbon\Carbon::now()->format('Y-m-d');
        });

        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
