<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateDatabaseCommand extends Command
{

    protected $signature = 'make:database {dbname} {connection}';

    public function handle()
    {
     try{
        $dbname = $this->argument('dbname');
        $connection = $this->argument('connection');
        \Config::set('database.connections.mysql.database', $connection);
        
        $databaseExists = \DB::select("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "."'".$dbname."'");

         if(empty($databaseExists)) {
            $this->info("Creating database: '$dbname' with connection: '$connection'");
            \DB::select('CREATE DATABASE ' . $dbname);
            $this->info("Database created");
            \Config::set('database.connections.mysql.database', $dbname);
         }
         else {
             $this->info("Database $dbname already exists for $connection connection");
         }
     }
     catch (\Exception $e){
         $this->error('ERROR CREATING DATABASE');
         $this->error($e->getMessage());
         throw $e;
     }
   }
}