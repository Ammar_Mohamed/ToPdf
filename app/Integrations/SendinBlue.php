<?php namespace App\Integrations;

use Log;

class SendinBlue
{
    const TESTS = [
        "mutablep.com"
    ];

    private $transactionalApiInstance = null;
    private $token = null; 

    public function init() {
        $this->token = env('EMAIL_TOKEN');
        $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', $this->token);
        $this->transactionalApiInstance = new \SendinBlue\Client\Api\TransactionalEmailsApi(
            // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
            // This is optional, `GuzzleHttp\Client` will be used as default.
            new \GuzzleHttp\Client(),
            $config
        );
        return $this;
    }

    public function sendv3(array $data)
    {

        $rules = [
            'to' 			=>	'required|array',
            'from' 			=>	'required_without:id|array',
            'html' 			=>	'required_without:id|string',
            'id'			=>	'required_without:html',
            'subject' 		=>	'string',
            'text'			=>	'string',
            'cc'			=>	'array',
            'bcc'			=>	'array',
            'reply_to'		=>	'array',
            'attachments'	=>	'array',
            'headers'		=>	'array',
            'inline_images'	=>	'array'
        ];
        
        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                \Log::error("[SendInBlue sendv3] Validator failed " . json_encode($error));
            }
            return false;
        }
        
        $sender = new \SendinBlue\Client\Model\SendSmtpEmailSender([
            'name' => $data['from'][1],
            'email' => $data['from'][0],
            // 'id' => ''
        ]);

        $to = [];

        foreach ($data['to'] as $email => $name) {
            $to[] = new \SendinBlue\Client\Model\SendSmtpEmailTo([
                'name' => $name,
                'email' => $email,
                // 'id' => ''
            ]);
        }

        $content = !empty($data['text']) ? $data['text'] : $data['subject'];
        $subject = $data['subject'];
        $templateId = $data['id'];
        $params = [];
        if(isset($data['attr'])) $params = $data['attr'];

        \Log::info("[SendInBlue sendv3] sendingV3 ".json_encode($params));

        $sendEmail = new \SendinBlue\Client\Model\SendSmtpEmail([
            'sender' => $sender,
            'to' => $to,
            'htmlContent' => $content,
            'subject' => $subject,
            'templateId' => $templateId,
            'params' => $params
        ]);
        
        try {
            $result = $this->transactionalApiInstance->sendTransacEmail($sendEmail);
            \Log::info("[SendInBlue sendv3] Transactional response ".json_encode($result));
            return $result;
        }catch(\Exception $e) {
            \Log::error("[SendInBlue sendv3] Error sending email".$e->getMessage());
        }
        return "Cant connect to sendinblue v3";
    }

    public static function sendEmailV3($data) {
        return (new self)->init()->sendv3($data);
    }
    
    public static function sendEmail($subject, $email, $name, $model, $attr = [], $company_id = null)
    {   

        if (env('APP_ENV', 'local') != 'production') {
            $domain = explode("@",$email)[1];
            if(!in_array($domain,self::TESTS)) {
                return true;
            }
        }
        $senderName = env('EMAIL_SENDER_NAME');
        $senderMail = env('EMAIL_SENDER_MAIL');

        \Log::info("[SendInBlue sendEmail] SENDER Name: ".$senderName . " SENDER Mail: ".$senderMail);
        $data = [
            'id'  => $model,
            'to' => array($email => $name),
            'from' => array($senderMail, $senderName),
            'subject' => $subject,
            'text' => '',
            'attr' => $attr,
            'attachment' => ''
        ];
        
        try {
            $result = self::sendEmailV3($data);
            \Log::info("[SendInBlue sendEmail] Transactional response ".json_encode($result));
            return true;
        } catch (\Exception $e) {
            \Log::error("[SendInBlue sendv3] Error sending email 2 ".$e->getMessage());
        }
        return false;
    }
    

    public static function sendExportEmail($data)
    {
        \Log::info("[SendInBlue sendResultsEmail] Send results email; email: " . $data['email']);

        return self::sendEmail(
            "Your export is ready",
            $data['email'],
            $data['email'],
            2,
            [
                'title' => $data['title'],
                'content' => $data['content'],
                'subject' => $data['subject'],
                "button_title" => $data['button_title'],
                "button_link" => $data['url'],
            ]
        );
    }


}
