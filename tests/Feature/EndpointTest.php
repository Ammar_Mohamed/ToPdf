<?php

namespace Tests\Feature;


use Tests\TestCase;

class EndpointTest extends TestCase
{
    
    /**
     * Test tabela page
     *
     * @return void
     */
    public function test_view_table_endpoint()
    {
        $response = $this->get('/tabela');
        $response->assertStatus(200);
    }


    /**
     * A basic test of json.
     *
     * @return void
     */
    public function test_endpoint_device_json()
    {

        $response = $this->json('GET', '/device/E62100890/inicio/01-08-2022/fim/02-08-2022');
        $response->assertStatus(200)
            ->assertJsonStructure([
                0 => [
                    "device",
                    "dia",
                    "periodo_noturno",
                    "rega_regular",
                    "total_consumption",
                    "dentro_quota",
                    "fora_quota",
                    "quota_max",
                    "total_pagar",
                    "hora0",
                    "valor0",
                    "hora1",
                    "valor1",
                    "hora2",
                    "valor2",
                    "hora3",
                    "valor3",
                    "hora4",
                    "valor4",
                    "hora5",
                    "valor5",
                    "hora6",
                    "valor6",
                    "hora7",
                    "valor7",
                    "hora8",
                    "valor8",
                    "hora9",
                    "valor9",
                    "hora10",
                    "valor10",
                    "hora11",
                    "valor11",
                    "hora12",
                    "valor12",
                    "hora13",
                    "valor13",
                    "hora14",
                    "valor14",
                    "hora15",
                    "valor15",
                    "hora16",
                    "valor16",
                    "hora17",
                    "valor17",
                    "hora18",
                    "valor18",
                    "hora19",
                    "valor19",
                    "hora20",
                    "valor20",
                    "hora21",
                    "valor21",
                    "hora22",
                    "valor22",
                    "hora23",
                    "valor23",
                ]
            ]);
    }
}
